import { useState } from "react";
import { useAuth } from "./Auth";
import { login } from "../core/request";
import FloatingLabel from 'react-bootstrap/FloatingLabel';
import Form from 'react-bootstrap/Form';

const Login = () => {

    const [username, setUsername] = useState("kminchelle");
    const [pass, setPass] = useState("0lelplR");
    const [error, setError] = useState("");
    const { saveAuth } = useAuth();

    const onLogin = (event) => {
        event.preventDefault();
        login(username, pass).then((response) => {
            setError("");
            saveAuth(response.data.token)
        }).catch((error) => {
            setError(error.response.data.message);
        })
    }
    return (
        <div className="container ">
            <div className="row ">`
               <div className="col-md-6 offset-md-3">
                    <div className="card my-5 bg-primary">
                           <div className="text-center">
                          <img src="https://img.freepik.com/free-vector/cross-bone-skull-wearing-gold-crown_43623-1392.jpg"
                             className="img-fluid profile-image-pic img-thumbnail rounded-circle my-3"
                              width="200px" alt="profile" />
                       </div>
            <FloatingLabel 
              controlId="floatingInput"
              label="Email address"
              className="mb-3"
            >
              <Form.Control type="email" placeholder="name@example.com" onChange={(e) => setUsername(e.target.value)}  />
            </FloatingLabel>
            <FloatingLabel controlId="floatingPassword" label="Password">
              <Form.Control type="password" placeholder="Password"  onChange={(e) => setPass(e.target.value)} />
            </FloatingLabel>
            <button onClick={onLogin} className="btn btn-color px-5 mb-5 w-100">Login</button>  
          </div>
          </div>
         </div>
        </div>
        );  
}


export default Login;
