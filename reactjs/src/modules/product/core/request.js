import axios from "axios";

const getListProduct = (limit = 10) => {
  return axios.get("/products", {
    params: { limit },
  });
};
const getListProductByCategory = (limit = 10, path = "") => {
  return axios.get(`/products/category/${path}`, {
    params: { limit },
  });
};

const getProductById = (id) => {
  return axios.get(`https://dummyjson.com/products${id}`);
};

const searchProduct = (params) => {
  return axios.get("https://dummyjson.com/products/search?", {
    params: params,
  });
};

const getCategories = () => {
  return axios.get("https://dummyjson.com/products/categories");
};
export {
  getListProduct,
  getProductById,
  searchProduct,
  getCategories,
  getListProductByCategory,
};
