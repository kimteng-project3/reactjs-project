import { useEffect, useState } from "react";
import {
  getCategories,
  getListProduct,
  getListProductByCategory,
  searchProduct,
} from "../core/request";
import { useNavigate } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Badge from "react-bootstrap/Badge";
import Pagination from "react-bootstrap/Pagination";

const ListProducts = () => {
  const [products, setProducts] = useState([]);
  const [active, setActive] = useState("All");
  const [categories, setCategories] = useState([]);
  const [searchText, setSearchText] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    getListProduct(100).then((response) => {
      setProducts(response.data.products);
    });

    getCategories().then((response) => {
      setCategories(response.data);
    });
  }, []);

  useEffect(() => {
    active === "All"
      ? getListProduct(100).then((response) => {
          setProducts(response.data.products);
        })
      : getListProductByCategory(100, active).then((response) => {
          setProducts(response.data.products);
        });
  }, [active]);

  const onSearch = () => {
    searchProduct({ q: searchText }).then((response) => {
      setProducts(response.data.products);
    });
  };

  return (
    <div className="container mt-5">
      <div className="w-100 d-flex justify-content-center mb-4">
        <Form className="d-flex justify-content-center px-5 w-50">
          <Form.Control
            type="search"
            placeholder="Search"
            className="me-2"
            aria-label="Search"
            onChange={(e) => setSearchText(e.target.value)}
          />
          <Button onClick={onSearch} variant="outline-success">
            Search
          </Button>
        </Form>
      </div>

      <div className="px-4">
        <div className="d-flex overflow-x-scroll w-100 pb-3">
          {categories.map((item, index) => (
            <button
              onClick={() => setActive(item)}
              key={index}
              className="px-4 py-2 fs-6 mx-2 "
              bg={active === item ? "primary" : "secondary"}
            >
              {item}
            </button>
          ))}
        </div>
      </div>

      <div className="d-flex flex-wrap justify-content-center">
        {products.length === 0 && <div>Not Found</div>}
        {products.map((product, index) => {
          return (
            <div
              onClick={() => navigate(`product/${product.id}`)}
              className="mx-2 border rounded-2 my-2"
              style={{}}
              key={index}
            >
              <img
                style={{
                  height: "200px",
                  width: "300px",
                }}
                className=""
                src={product.thumbnail}
              />
              <p className="border-top px-2 bg-primary">
                <Button variant="dark">{product.title}</Button>
              </p>
            </div>
          );
        })}
      </div>
      <Pagination className="justify-content-center">
        <Pagination.First />
        <Pagination.Prev />
        <Pagination.Item>{1}</Pagination.Item>
        <Pagination.Ellipsis />

        <Pagination.Item>{10}</Pagination.Item>
        <Pagination.Item>{11}</Pagination.Item>
        <Pagination.Item active>{12}</Pagination.Item>
        <Pagination.Item>{13}</Pagination.Item>
        <Pagination.Item disabled>{14}</Pagination.Item>

        <Pagination.Ellipsis />
        <Pagination.Item>{20}</Pagination.Item>
        <Pagination.Next />
        <Pagination.Last />
      </Pagination>
    </div>
  );
};

export default ListProducts;
