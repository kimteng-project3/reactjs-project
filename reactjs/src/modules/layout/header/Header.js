import Nav from "react-bootstrap/Nav";
import { useAuth } from "../../auth/components/Auth";

const Header = () => {
  const { logout } = useAuth();

  return (
    <Nav fill variant="tabs" defaultActiveKey="/home">
      <Nav.Item>
        <Nav.Link href="/">Home</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link href="/blog">Blog</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link href="/contact">Comment</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link variant="danger" onClick={logout}>
          Logout
        </Nav.Link>
      </Nav.Item>
    </Nav>
  );
};

export default Header;
